/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.*;

/**
 *
 * @author lalo
 */
public class AnalizadorSintacticoLogic {

    public static int estado = 0;
    public static List<Integer> pila = new LinkedList<>();
    public static String[] cadena;
    public static List valor = new LinkedList<>();
    public static String accion = "";
    public static final int red[] = {0, 3, 3, 1, 3, 3, 1, 3, 3, 1, 2, 3, 1, 1, 1, 1};
    public static final String con[] = {".", "A", "A", "A", "B", "B", "B", "C", "C", "C", "D", "D", "D", "D", "D", "D"};
    public static final String TabLogic[][] = {
        //        $      &      |      ==    !=      >      <      !      (      )      V      F      I      N      A      B      C      D
        //                             #      @
        /*00 */{"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "1", "2", "3", "4"},
        /*01*/ {"AC", "S11", "S12", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*02*/ {"R3", "R3", "R3", "S13", "S14", "er", "er", "er", "er", "R3", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*03*/ {"R6", "R6", "R6", "R6", "R6", "S15", "S16", "er", "er", "R6", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*04*/ {"R9", "R9", "R9", "R9", "R9", "R9", "R9", "er", "er", "R9", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*05*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "er", "er", "17"},
        /*06*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "18", "2", "3", "4"},
        /*07*/ {"R12", "R12", "R12", "R12", "R12", "R12", "R12", "er", "er", "R12", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*08*/ {"R13", "R13", "R13", "R13", "R13", "R13", "R13", "er", "er", "R13", "er", "S8", "er", "er", "er", "er", "er", "er"},
        /*09*/ {"R14", "R14", "R14", "R14", "R14", "R14", "R14", "er", "er", "R14", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*10*/ {"R15", "R15", "R15", "R15", "R15", "R15", "R15", "er", "er", "R15", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*11*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "19", "3", "4"},
        /*12*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "20", "3", "4"},
        /*13*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "er", "21", "4"},
        /*14*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "er", "22", "4"},
        /*15*/ {"er", "er", "er", "er", "er", "S3", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "er", "er", "23"},
        /*16*/ {"er", "er", "er", "er", "er", "er", "er", "S5", "S6", "er", "S7", "S8", "S9", "S10", "er", "er", "er", "24"},
        /*17*/ {"R10", "R10", "R10", "R10", "R10", "R10", "R10", "S5", "S6", "R10", "S7", "S8", "S9", "S10", "er", "2", "3", "4"},
        /*18*/ {"er", "S11", "S12", "er", "er", "er", "er", "er", "er", "S25", "er", "er", "er", "er", "6", "er", "er", "er"},
        /*19*/ {"R1", "R1", "R1", "S13", "S14", "er", "er", "er", "er", "R1", "er", "er", "er", "er", "er", "11", "er", "er"},
        /*20*/ {"er", "R2", "R2", "S13", "S14", "er", "er", "er", "er", "R2", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*21*/ {"R4", "R4", "R4", "R4", "R4", "er", "er", "er", "er", "R4", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*22*/ {"er", "R5", "R5", "R5", "R5", "er", "er", "er", "er", "R5", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*23*/ {"er", "R7", "R7", "R7", "R7", "R7", "R7", "er", "er", "R7", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*24*/ {"R8", "R8", "R8", "R8", "R8", "R8", "R8", "er", "er", "R8", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*25*/ {"R11", "R11", "R11", "R11", "R11", "R11", "R11", "er", "er", "R11", "er", "er", "er", "er", "er", "er", "er", "er"},
        /*ER*/ {"er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er", "er"}};

    public static Boolean IsExpLogic(String cad, int lnn) {
        valor.clear();
        cad = AnalizadorSintacticoMath.AjusteID(cad);
        pila.add(0);

        // System.out.println("Cadena: " + (char) 27 + "[31m" + cad);
        cadena = cad.split(" ");
        while (cadena.length > 0) {

            accion = getAction(cadena[0]);
            if (accion.startsWith("S")) {
                //----------------------------------------------------------------
                pila.add(Integer.parseInt(accion.split("S")[1]));
                if (!cadena[0].startsWith("id")) {
                    valor.add(cadena[0]);
                    //System.out.println("add: " + cadena[0]);
                } else {
                    valor.add(TabVar.getValueId(cadena[0]));
                    //System.out.println("add: " + TabVar.getValue(cadena[0]));
                }
                cadena = Arrays.copyOfRange(cadena, 1, cadena.length);
                //----------------------------------------------------------------
            } else if (accion.startsWith("R")) {
                //----------------------------------------------------------------
                int y = Integer.parseInt(accion.split("R")[1]);
                int a = red[y];
                for (int x = 0; x < a; x++) {
                    pila.remove(pila.size() - 1);
                }
                pila.add(Integer.parseInt(getAction(con[y])));
                if (y == 1) {//and
                    //System.out.println("reducir and");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 3).toString());
                        boolean B = Boolean.valueOf(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A & B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con &\n";
                    }
                } else if (y == 2) {//or
                    //System.out.println("reducir or");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 3).toString());
                        boolean B = Boolean.valueOf(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A | B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con |\n";
                    }
                } else if (y == 4) {//==
                    //System.out.println("reducir ==");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 3).toString());
                        boolean B = Boolean.valueOf(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A == B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con ==\n";
                    }
                } else if (y == 5) {//!=
                    //System.out.println("reducir !=");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 3).toString());
                        boolean B = Boolean.valueOf(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A != B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con !=\n";
                    }
                } else if (y == 7) {//>
                    //System.out.println("reducir >");
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A > B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con >\n";
                    }
                } else if (y == 8) {//<
                    //System.out.println("reducir <");
                    try {
                        int A = Integer.parseInt(valor.get(valor.size() - 3).toString());
                        int B = Integer.parseInt(valor.get(valor.size() - 1).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A < B);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con <\n";
                    }
                } else if (y == 10) {//!
                    //System.out.println("reducir !");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 2).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(!A);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon: Los datos no se pueden comparar con !\n";
                    }

                } else if (y == 11) {//()
                    //System.out.println("reducir ( )");
                    try {
                        boolean A = Boolean.valueOf(valor.get(valor.size() - 2).toString());
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.remove(valor.size() - 1);
                        valor.add(A);
                    } catch (Exception e) {
                        Compilador.x += "==> Linea: " + lnn + " Razon:???\n";
                    }

                }

                //----------------------------------------------------------------
            } else if (accion.equals("er")) {
                return null;
            } else if (accion.equals("AC")) {
                if (valor.get(0) != "(") {
                    //System.out.println("resultado: " + Boolean.valueOf(valor.get(0).toString()));
                    return Boolean.valueOf(valor.get(0).toString());
                } else {
                    //System.out.println("resultado: " + Boolean.valueOf(valor.get(1).toString()));
                    return Boolean.valueOf(valor.get(1).toString());
                }
            }
            /*
            System.out.print("" + valor + "\t\t" + pila + "\t\t" + accion + "\t\t");
            for (String ca : cadena) {
                System.out.print(ca + " ");
            }
            System.out.println("");//*/
        }
        return null;
    }

    public static String getAction(String l) {
        estado = pila.get(pila.size() - 1);
        //        $      &      |      ==    !=      >      <      !      (      )      V      F      I      N      A      B      C      D
        int s = (l.equals("$")) ? 0
                : (l.equals("&") | l.equals("and")) ? 1
                        : (l.equals("|") | l.equals("or")) ? 2
                                : (l.equals("==")) ? 3
                                        : (l.equals("!=")) ? 4
                                                : (l.equals(">")) ? 5
                                                        : (l.equals("<")) ? 6
                                                                : (l.equals("!")) ? 7
                                                                        : (l.equals("(")) ? 8
                                                                                : (l.equals(")")) ? 9
                                                                                        : (l.equals("true")) ? 10
                                                                                                : (l.equals("false")) ? 11
                                                                                                        : (l.startsWith("id")) ? 12
                                                                                                                : (AnalizadorSintacticoMath.isNumber(l)) ? 13
                                                                                                                        : (l.equals("A")) ? 14
                                                                                                                                : (l.equals("B")) ? 15
                                                                                                                                        : (l.equals("C")) ? 16
                                                                                                                                                : (l.equals("D")) ? 17
                                                                                                                                                        : -1;
        String r = "er";
        try {
            return TabLogic[estado][s];
        } catch (Exception eee) {
            return r;
        }
    }

    static Boolean IsExpLogic(String AjustarOperandosLogic) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
