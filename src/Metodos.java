
import java.io.BufferedReader;

public class Metodos {

    public static String clearMoreSpaces(String l) {
        for (int i = 1; i < l.length(); i++) {
            l = l.replaceAll("  ", " ");
        }
        return l;
    }

    public static String clearComments(String l) {
        String a[] = l.split(";");
        if (l.contains(";") && a.length > 1) {
            return a[0];
        } else {
            return l;
        }
    }

    public static String clearTabs(String l) {
        return l.replaceAll("\t", "");
    }

    public static String clearSpaceAfterLetters(String l) {
        String s;
        while (true) {
            s = "";
            if (l.length() > 1 && l.charAt(0) == ' ') {

                for (int j = 1; j < l.length(); j++) {
                    s += l.charAt(j);
                }
                l = s;
            } else {
                break;
            }
        }
        return l;
    }

    public static String clearSpaceBeforeLetters(String l) {
        String s;
        while (true) {
            s = "";
            if (l.length() > 1 && l.charAt(l.length() - 1) == ' ') {

                for (int j = 0; j < l.length() - 1; j++) {
                    s += l.charAt(j);
                }
                l = s;
            } else {
                break;
            }
        }
        return l;
    }

    public static String clearLineOnlyWithSpaces(String l) {
        return (l.isEmpty()) ? "" : (l.equals(" ")) ? "" : l;
    }

    public static String correctionSigs(String l) {

        l = l
                .replace("+", " + ")
                .replace("-", " - ")
                .replace("*", " * ")
                .replace("/", " / ")
                .replace(")", " ) ")
                .replace("(", " ( ")
                .replace("!", " ! ")
                .replace("! =", " != ")
                .replace("!=", " != ")
                .replace("and", " and ")
                .replace("or", " or ")
                .replace(">", " > ")
                .replace("<", " < ")
                .replace("=", " = ")
                .replace("==", " == ");
        return l.replaceAll("=\\s*=", " == ");
    }

    public static boolean isPrintable(String a) {
        if ((a.startsWith(Character.toString('"')) && a.endsWith(Character.toString('"')))
                || (a.startsWith(" " + Character.toString('"')) && a.endsWith(Character.toString('"')))
                || (a.startsWith(" " + Character.toString('"')) && a.endsWith(Character.toString('"') + " "))) {
            return true;
        }
        return TabVar.isVariable(a);
    }

    public static void analizeLine(String l, int lnn) {
        String ln[] = l.split(" ");
        String var = "";//a = leer()

        if ((l.contains(" + ") || l.contains(" - ") || l.contains(" * ") || l.contains(" / "))) {
            Integer x;
            if ((x = AnalizadorSintacticoMath.IsExpMat(AjustarOperandosMath(l), lnn)) == null) {
                Compilador.x += "==> Linea: " + lnn + " Razon: La exprecion matematica es invalida.\n";
            } else {
                TabVar.modify(l.split("=")[0].trim(), "" + x, lnn);
                Compilador.a3d.Escribir("" + TabVar.getIdConsec("" + l.split("=")[0].trim()) + " = t" + (Compilador.temp - 1));
                Compilador.a3d.Escribir("");
            }
        } else if ((l.contains(" | ") || (l.contains(" & ")) || (l.contains(" < ")) || (l.contains(" > ")) || (l.contains(" ! ")) || (l.contains(" == ")) || (l.contains(" != "))) && !l.startsWith("if") && !l.startsWith("while")) {
            try {

                Boolean a;
                if ((a = AnalizadorSintacticoLogic.IsExpLogic(AjustarOperandosLogic(l), lnn)) == null) {
                    Compilador.x += "==> Linea: " + lnn + " Razon: La exprecion logica es invalida.\n";
                } else {
                    TabVar.modify(l.split("=")[0].trim(), "" + a, lnn);
                }
            } catch (Exception e) {
            }
        } else if (l.startsWith("if (") && l.endsWith(")")) {
            l = l.replace("if (", "");
            l = l.substring(0, l.length() - 2);
            if (AnalizadorSintacticoLogic.IsExpLogic(AjustarOperandosLogic(l), lnn) == null) {
                Compilador.x += "==> Linea: " + lnn + " Razon: La exprecion logica es invalida.\n";
            }
        } else if (l.startsWith("while (") && l.endsWith(")")) {
            l = l.replace("while (", "");
            l = l.substring(0, l.length() - 2);
            if (AnalizadorSintacticoLogic.IsExpLogic(AjustarOperandosLogic(l), lnn) == null) {
                Compilador.x += "==> Linea: " + lnn + " Razon: La exprecion logica es invalida.\n";
            }
        } else if (l.startsWith("imprimir ( ") && l.endsWith(" )")) {
            if (isPrintable(l.replace("imprimir ( ", "").replace(" )", ""))) {
                var = "IMPRESION";
                if (l.split(" ")[2].startsWith("\"")) {
                    Compilador.a3d.Escribir("imprimir " + l.split(" ")[2]);
                    Compilador.a3d.Escribir("");
                } else {
                    Compilador.a3d.Escribir("imprimir " + TabVar.getIdConsec(l.replace("imprimir ( ", "").replace(" )", "")));
                    Compilador.a3d.Escribir("");
                }
            } else {
                Compilador.x += "==> Linea: " + lnn + " Razon: El dato no es imprimible.\n";
                var = "";
            }
        } else if ((ln.length == 5 && ln[2].equals("leer") && ln[3].equals("(") && ln[4].equals(")"))) {
            if (TabVar.isVariable(ln[0])) {
                Compilador.a3d.Escribir("leer " + TabVar.getIdConsec(ln[0]));
                Compilador.a3d.Escribir("");
                //System.out.println("asm: leer " + ln[0]);
                var = "LECTURA";
            } else {
                Compilador.x += "==> Linea: " + lnn + " Razon: La variable ~" + ln[0] + "~ no fue declarada.\n";
                var = "";
            }
        } else if (ln.length == 3 && TabVar.isVariable(ln[0]) && !TabVar.isVariable(ln[2])) {
            var = "ASIGNACION";
        } else if (ln.length == 3 && TabVar.isVariable(ln[0]) && TabVar.isVariable(ln[2])) {
            var = "ASIGNACIONOTRAVAR";
        } else if (l.startsWith(TabPalRes.PalabrasReservadas[0]) || l.startsWith(TabPalRes.PalabrasReservadas[1]) || l.startsWith(TabPalRes.PalabrasReservadas[2]) || l.startsWith(TabPalRes.PalabrasReservadas[3])) {
            if (ln.length == 4) {
                var = "DECLARACION_EX";
            } else if (ln.length == 2) {
                var = "DECLARACION_IN";
            }
        } else if (l.length() > 0) {
        } else if (l.equals("")) {
        } else {
            Compilador.x += "==> Linea: " + lnn + " Razon: Desconocida.\n";

        }
        switch (var) {
            case "DECLARACION_EX"://que se especifica
                TabVar.addVar(ln[1], ln[0], ln[3], lnn);
                break;
            case "DECLARACION_IN"://que no se especifica
                String[] n = TabVar.getTod();
                if (ln[0].equals(n[0]))//ent
                {
                    TabVar.addVar(ln[1], ln[0], "0", lnn);
                } else if (ln[0].equals(n[1]))//cad
                {
                    TabVar.addVar(ln[1], ln[0], "\"\"", lnn);
                } else if (ln[0].equals(n[2]))//char
                {
                    TabVar.addVar(ln[1], ln[0], "''", lnn);
                } else if (ln[0].equals(n[3]))//boolean
                {
                    TabVar.addVar(ln[1], ln[0], "false", lnn);
                }
                break;
            case "ASIGNACION":
                TabVar.modify(ln[0], ln[2], lnn);
                Compilador.a3d.Escribir(TabVar.getIdConsec(ln[0]) + " = " + ln[2] + "\n");
                break;
            case "ASIGNACIONOTRAVAR":
                TabVar.copyValue(ln[0], ln[2], lnn);
                break;
            case "LECTURA":
                //System.out.println("Aqui lee...");
                break;
            case "IMPRESION":
                //System.out.println("Aqui imprime...");
                break;
            case "":
                break;
        }
    }

    static boolean isData(String ln) {
        if (ln.startsWith(Character.toString('"')) && ln.endsWith(Character.toString('"'))) {
            return true;
        } else if (ln.startsWith("'") && ln.endsWith("'")) {
            return true;
        } else if (ln.matches("[-]?[0-9]*")) {
            return true;
        } else if (ln.equals("true") || ln.equals("false")) {
            return true;
        }
        return true;
    }

    private static String AjustarOperandosMath(String iiI$) {
        iiI$ = iiI$.split("=")[1];
        try {
            for (String[] x : TabVar.getTabVar()) {
                iiI$ = iiI$.replaceAll("[ ]+" + x[0] + "[ ]*", " " + x[3] + " ");
            }
        } catch (Exception e) {
        }
        return (iiI$ + " $").trim();

    }

    private static String AjustarOperandosLogic(String iiI$) {

        if (iiI$.contains(" = ")) {
            iiI$ = iiI$.split(" = ")[1];
        }
        iiI$ = iiI$.replace("! =", "!=");
        iiI$ = iiI$.replace("= =", "==");
        for (String[] x : TabVar.getTabVar()) {
            iiI$ = iiI$.replaceAll("[ ]+" + x[0] + "[ ]*", " " + x[3] + " ");
        }
        iiI$ = (iiI$ + " $ ").trim();
        return iiI$;
    }

    static void contadorDeIdsTemporales(BufferedReader br) {
        String linea;
        int n = 0;
        try {
            while ((linea = br.readLine()) != null) {
                ComprobacionDeLlaves.evaluaLlave(linea);
                System.out.println((n + 1) + "\tcontando\t:\t" + linea);
            }
        } catch (Exception e) {
        }

    }
}
