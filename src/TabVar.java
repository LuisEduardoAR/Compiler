
/**
 *
 * @author lalo
 * @param FunguiCompiler
 *
 */
public class TabVar {

    private static final String tod[] = new String[]{TabPalRes.PalabrasReservadas[0], TabPalRes.PalabrasReservadas[1], TabPalRes.PalabrasReservadas[2], TabPalRes.PalabrasReservadas[3]};
    private static String[][] TablaVariables;
    private static int c = 0;

    public static boolean isValidID(String id) {
        return id.matches("[a-zA-Z]{1}[a-zA-Z0-9]*");
    }

    public static void addVar(String n, String t, String v, int ln) {
        String D = "", f;
        if (!varExist(n)) {
            if (!TabPalRes.isReservedWord(n)) {
                if (isValidID(n)) {
                    if (isTypeOfDate(t)) {
                        if (isValidValueForTypeOfDate(t, v)) {
                            if (TablaVariables == null) {
                                TablaVariables = new String[1][4];
                                TablaVariables[0] = new String[]{n, t, v, "id" + c};
                                c++;
                            } else {
                                String temp[][] = new String[TablaVariables.length + 1][4];

                                for (int i = 0; i < TablaVariables.length; i++) {
                                    System.arraycopy(TablaVariables[i], 0, temp[i], 0, TablaVariables[0].length);
                                }
                                temp[TablaVariables.length] = new String[]{n, t, v, "id" + c};
                                c++;
                                TablaVariables = temp;
                            }
                        } else if (isValidValueForTypeOfDate(t, getValue(v))) {
                            if (TablaVariables == null) {
                                TablaVariables = new String[1][4];
                                TablaVariables[0] = new String[]{n, t, getValue(v), "id" + c};
                                c++;
                            } else {
                                String temp[][] = new String[TablaVariables.length + 1][4];

                                for (int i = 0; i < TablaVariables.length; i++) {
                                    System.arraycopy(TablaVariables[i], 0, temp[i], 0, TablaVariables[0].length);
                                }
                                temp[TablaVariables.length] = new String[]{n, t, getValue(v), "id" + c};
                                c++;
                                TablaVariables = temp;
                            }
                        } else {
                            D = "==> Linea: " + ln + " Razon: El valor de " + v + " no se puede asignar a una variable de tipo " + t + ".\n";
                        }
                    } else {
                        D = "==> Linea: " + ln + " Razon: El tipo de dato ~" + t + "~ no es un tipo de dato valido.\n";
                    }
                } else {
                    D = "==> Linea: " + ln + " Razon: La variable ~" + n + "~ no es un id valido.\n";
                }
            } else {
                D = "==> Linea: " + ln + " Razon: La palabra reservada ~" + n + "~ no puede ser un id.\n";
            }
        } else {
            D = "==> Linea: " + ln + " Razon: La variable ~" + n + "~ ya fue declarada.\n";
        }
        //(Compilador.x).concat(D);
        /**/
        Compilador.x += D;
    }

    public static boolean isValidValueForTypeOfDate(String t, String v) {
        try {
            if (t.equals(tod[0])) {//entero
                return v.matches("[-]?[0-9]+");
            } else if (t.equals(tod[1])) {//cad
                return v.matches("\"([^\"]*)\"");
            } else if (t.equals(tod[3])) {//caracter
                return v.matches("true|false");
            } else if (t.equals(tod[2])) { //booleano
                return v.matches("[']?[.]?[']?");
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean varExist(String var) {
        if (TablaVariables != null) {
            for (String[] TablaVariable : TablaVariables) {
                if (TablaVariable[0].equals(var)) {
                    return true;
                }
            }
            return false;
        }
        return false;
    }

    public static String[][] getTabVar() {
        return TablaVariables;
    }

    public static String[] getTod() {
        return tod;
    }

    public static boolean isTypeOfDate(String t) {
        for (String d : tod) {
            if (d.equals(t)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isVariable(String a) {
        if (TablaVariables != null) {
            for (String[] tv : TablaVariables) {
                if (tv[0].equals(a)) {
                    return true;
                }
            }
        }
        return false;
    }

    static void modify(String n, String v, int ln) {
        for (int i = 0; i < TablaVariables.length; i++) {
            if (TablaVariables[i][0].equals(n)) {
                if (isValidValueForTypeOfDate(TablaVariables[i][1], v)) {
                    TablaVariables[i][2] = v;
                    //Compilador.a3d.Escribir("                    id" + i + " = " + v + "\n");
                    break;
                } else {
                    Compilador.x += "==> Linea: " + ln + " Razon: El valor '" + v + "'. no se puede copiar a una variable de tipo de dato " + TablaVariables[i][1] + ".\n";
                }
            }
        }
    }

    static void copyValue(String n, String v, int ln) {
        modify(n, TabVar.getValue(v), ln);
    }

    static String getValue(String var) {
        try {
            if (TablaVariables.length != 0) {
                for (String[] a : TablaVariables) {
                    if (a[0].equals(var)) {
                        System.out.println("retornando valor: " + a[2]);
                        return a[2];
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }

    static String getValueId(String var) {
        try {
            if (TablaVariables.length != 0) {
                for (String[] a : TablaVariables) {
                    if (a[3].equals(var)) {
                        //System.out.println("retornando valor: "+a[2]);
                        return a[2];
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }

    static String getIdConsec(String var) {
        try {
            if (TablaVariables.length != 0) {
                for (String[] a : TablaVariables) {
                    if (a[0].equals(var)) {
                        // System.out.println("retornando valor: " + a[3] + " de " + a[0]);
                        return a[3];
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }

    static String getType(String id) {
        try {
            if (TablaVariables.length != 0) {
                for (String[] a : TablaVariables) {
                    if (a[3].equals(id)) {
                         //System.out.println("retornando valor: " + a[1] + " de " + a[3]);
                        return a[1];
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }
}
